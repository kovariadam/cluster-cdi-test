package cz.vutbr.fit.bp.xkovar57.test;

import cz.vutbr.fit.bp.xkovar57.test.data.ClusteredBean;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Logger;

/**
 * @author Adam Kovari <akovari @ redhat.com>
 */
@WebServlet(urlPatterns = "/set")
public class SetServlet extends HttpServlet {
    @Inject
    private ClusteredBean clusteredBean;

    @Inject
    private Logger logger;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        clusteredBean.setValue("XXX" + new SimpleDateFormat().format(Calendar.getInstance().getTime()));
        logger.info("ClusterBean's current value: " + clusteredBean.getValue());
    }
}
