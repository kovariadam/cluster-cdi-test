package cz.vutbr.fit.bp.xkovar57.test.data;

import cz.vutbr.fit.bp.xkovar57.annotations.ClusterScoped;
import cz.vutbr.fit.bp.xkovar57.annotations.ClusteredOperation;

import java.io.Serializable;

/**
 * @author Adam Kovari <akovari @ redhat.com>
 */
@ClusterScoped
public class ClusteredBean implements Serializable {
    private static final long serialVersionUID = 8204532302582628391L;
    private String value;

    @ClusteredOperation
    public String getValue() {
        return value;
    }

    @ClusteredOperation
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ClusteredBean{" +
                "value='" + value + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClusteredBean that = (ClusteredBean) o;

        if (value != null ? !value.equals(that.value) : that.value != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }
}
